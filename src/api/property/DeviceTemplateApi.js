import Request from '@/utils/request-util';

/**
 * 公司资源-设备模板api
 *
 * @author cancan
 * @date 2022/03/04 21:15
 */
export class DeviceTemplateApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceTemplate/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static add(params) {
    return Request.post('/deviceTemplate/add', params);
  }

  static async getList(params) {
    return await Request.get('/deviceTemplate/list', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static edit(params) {
    return Request.post('/deviceTemplate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static delete(params) {
    return Request.post('/deviceTemplate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceTemplate/detail', params);
  }
}
