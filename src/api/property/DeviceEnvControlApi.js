import Request from '@/utils/request-util';

/**
 * 环控设备api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControl/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControl/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControl/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControl/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControl/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceEnvControl/detailByRecordId', {"recordId" : recordId});
  }
}
