import Request from '@/utils/request-util';

/**
 * 公司资源-喷淋主板设备-命令下发api
 *
 * @author cancan
 * @date 2022/07/23 22:24
 */
export class DeviceSpraymainboardCmdApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSpraymainboardCmd/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static add(params) {
    return Request.post('/deviceSpraymainboardCmd/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static edit(params) {
    return Request.post('/deviceSpraymainboardCmd/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static delete(params) {
    return Request.post('/deviceSpraymainboardCmd/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSpraymainboardCmd/detail', params);
  }

  static workingDetail(params) {
    return Request.getAndLoadData('/deviceSpraymainboardCmd/workingDetail', params);
  }
}
