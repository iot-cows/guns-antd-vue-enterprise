import Request from '@/utils/request-util';

/**
 * 水槽设备告警数据api
 *
 * @author cancan
 * @date 2022/12/25 16:20
 */
export class DeviceSinkWarnDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSinkWarnData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static add(params) {
    return Request.post('/deviceSinkWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static edit(params) {
    return Request.post('/deviceSinkWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static delete(params) {
    return Request.post('/deviceSinkWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSinkWarnData/detail', params);
  }
}
