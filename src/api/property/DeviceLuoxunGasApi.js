import Request from '@/utils/request-util';

/**
 * 逻讯-气体感知设备api
 *
 * @author cancan
 * @date 2022/09/29 01:32
 */
export class DeviceLuoxunGasApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunGas/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static add(params) {
    return Request.post('/deviceLuoxunGas/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static edit(params) {
    return Request.post('/deviceLuoxunGas/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static delete(params) {
    return Request.post('/deviceLuoxunGas/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunGas/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunGas/detailByRecordId', {"recordId" : recordId});
  }
}
