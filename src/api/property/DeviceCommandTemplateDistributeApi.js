import Request from '@/utils/request-util';

/**
 * 分发任务api
 *
 * @author cancan
 * @date 2024/10/03 21:26
 */
export class DeviceCommandTemplateDistributeApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceCommandTemplateDistribute/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static add(params) {
    return Request.post('/deviceCommandTemplateDistribute/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static edit(params) {
    return Request.post('/deviceCommandTemplateDistribute/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static delete(params) {
    return Request.post('/deviceCommandTemplateDistribute/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCommandTemplateDistribute/detail', params);
  }
}
