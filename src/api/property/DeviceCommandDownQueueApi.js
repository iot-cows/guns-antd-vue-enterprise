import Request from '@/utils/request-util';

/**
 * 设备下发命令队列api
 *
 * @author cancan
 * @date 2023/05/11 21:45
 */
export class DeviceCommandDownQueueApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCommandDownQueue/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/11 21:45
   */
  static add(params) {
    return Request.post('/deviceCommandDownQueue/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/11 21:45
   */
  static edit(params) {
    return Request.post('/deviceCommandDownQueue/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/11 21:45
   */
  static delete(params) {
    return Request.post('/deviceCommandDownQueue/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/11 21:45
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCommandDownQueue/detail', params);
  }

  static workingDetail(params) {
    return Request.getAndLoadData('/deviceCommandDownQueue/workingDetail', params);
  }

  static workingDetail2(params) {
    return Request.getAndLoadData('/deviceCommandDownQueue/workingDetail2', params);
  }
}
