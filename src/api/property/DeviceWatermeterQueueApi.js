import Request from '@/utils/request-util';

/**
 * 水表下发命令队列api
 *
 * @author cancan
 * @date 2023/05/08 23:28
 */
export class DeviceWatermeterQueueApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceWatermeterQueue/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/08 23:28
   */
  static add(params) {
    return Request.post('/deviceWatermeterQueue/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/08 23:28
   */
  static edit(params) {
    return Request.post('/deviceWatermeterQueue/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/08 23:28
   */
  static delete(params) {
    return Request.post('/deviceWatermeterQueue/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/08 23:28
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceWatermeterQueue/detail', params);
  }

  static workingDetail(params) {
    return Request.getAndLoadData('/deviceWatermeterQueue/workingDetail', params);
  }
}
