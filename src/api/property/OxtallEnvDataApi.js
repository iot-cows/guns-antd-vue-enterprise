import Request from '@/utils/request-util';

/**
 * 牧场环境采集数据api
 *
 * @author cancan
 * @date 2023/07/08 18:04
 */
export class OxtallEnvDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/oxtallEnvData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/07/08 18:04
   */
  static add(params) {
    return Request.post('/oxtallEnvData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/07/08 18:04
   */
  static edit(params) {
    return Request.post('/oxtallEnvData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/07/08 18:04
   */
  static delete(params) {
    return Request.post('/oxtallEnvData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/07/08 18:04
   */
  static detail(params) {
    return Request.getAndLoadData('/oxtallEnvData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/oxtallEnvData/list', params);
  }
}
