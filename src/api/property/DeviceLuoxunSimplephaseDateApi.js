import Request from '@/utils/request-util';

/**
 * 电日使用量统计api
 *
 * @author cancan
 * @date 2022/10/07 20:59
 */
export class DeviceLuoxunSimplephaseDateApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseDate/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static add(params) {
    return Request.post('/deviceLuoxunSimplephaseDate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static edit(params) {
    return Request.post('/deviceLuoxunSimplephaseDate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static delete(params) {
    return Request.post('/deviceLuoxunSimplephaseDate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseDate/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseDate/list', params);
  }
}
