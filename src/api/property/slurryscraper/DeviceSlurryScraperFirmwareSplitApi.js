import Request from '@/utils/request-util';

/**
 * 固件拆分后的文件api
 *
 * @author cancan
 * @date 2024/06/08 11:30
 */
export class DeviceSlurryScraperFirmwareSplitApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperFirmwareSplit/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperFirmwareSplit/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperFirmwareSplit/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperFirmwareSplit/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperFirmwareSplit/detail', params);
  }
}
