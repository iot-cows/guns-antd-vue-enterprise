import Request from '@/utils/request-util';

/**
 * 每天用电量计算api
 *
 * @author cancan
 * @date 2024/10/13 15:21
 */
export class DeviceSlurryScraperTodayConsumptionApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayConsumption/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperTodayConsumption/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperTodayConsumption/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperTodayConsumption/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayConsumption/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayConsumption/list', params);
  }

  static findCalenAndComsumptionList(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayConsumption/calenAndComsumption/list', params);
  }

  static findCalenAndComsumptionPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayConsumption/calenAndComsumption/page', params);
  }




}
