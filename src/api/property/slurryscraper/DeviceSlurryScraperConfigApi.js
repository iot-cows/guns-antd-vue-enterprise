import Request from '@/utils/request-util';

/**
 * 刮粪机参数设置api
 *
 * @author cancan
 * @date 2024/06/08 11:30
 */
export class DeviceSlurryScraperConfigApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperConfig/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperConfig/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperConfig/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperConfig/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperConfig/detail', params);
  }
}
