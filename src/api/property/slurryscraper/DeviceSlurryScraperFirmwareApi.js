import Request from '@/utils/request-util';

/**
 * 固件信息api
 *
 * @author cancan
 * @date 2024/06/08 11:30
 */
export class DeviceSlurryScraperFirmwareApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperFirmware/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperFirmware/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperFirmware/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperFirmware/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperFirmware/detail', params);
  }

  static list(params) {
    return Request.get('/deviceSlurryScraperFirmware/list', params);
  }
}
