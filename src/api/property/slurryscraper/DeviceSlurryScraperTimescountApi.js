import Request from '@/utils/request-util';

/**
 * 统计单次耗电量api
 *
 * @author cancan
 * @date 2024/10/19 17:37
 */
export class DeviceSlurryScraperTimescountApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTimescount/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/19 17:37
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperTimescount/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/19 17:37
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperTimescount/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/19 17:37
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperTimescount/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/19 17:37
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTimescount/detail', params);
  }
  static list(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTimescount/list', params);
  }
}
