import Request from '@/utils/request-util';

/**
 * 刮粪机api
 *
 * @author cancan
 * @date 2024/06/08 11:30
 */
export class DeviceSlurryScraperApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraper/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static add(params) {
    return Request.post('/deviceSlurryScraper/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraper/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraper/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraper/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceSlurryScraper/detailByRecordId', {"recordId" : recordId});
  }
}
