import Request from '@/utils/request-util';

/**
 * 固件升级进度表api
 *
 * @author cancan
 * @date 2024/06/08 11:30
 */
export class DeviceSlurryScraperUpgradeApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperUpgrade/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperUpgrade/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperUpgrade/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperUpgrade/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/06/08 11:30
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperUpgrade/detail', params);
  }
}
