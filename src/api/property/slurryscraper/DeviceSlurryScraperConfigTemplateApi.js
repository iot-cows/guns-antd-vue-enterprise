import Request from '@/utils/request-util';

/**
 * 刮粪机参数设置模板api
 *
 * @author cancan
 * @date 2024/10/05 21:26
 */
export class DeviceSlurryScraperConfigTemplateApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperConfigTemplate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/05 21:26
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperConfigTemplate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/05 21:26
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperConfigTemplate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/05 21:26
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperConfigTemplate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/05 21:26
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperConfigTemplate/detail', params);
  }
}
