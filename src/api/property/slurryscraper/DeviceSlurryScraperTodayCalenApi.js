import Request from '@/utils/request-util';

/**
 * 每天清粪次数api
 *
 * @author cancan
 * @date 2024/10/13 15:21
 */
export class DeviceSlurryScraperTodayCalenApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayCalen/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static add(params) {
    return Request.post('/deviceSlurryScraperTodayCalen/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static edit(params) {
    return Request.post('/deviceSlurryScraperTodayCalen/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static delete(params) {
    return Request.post('/deviceSlurryScraperTodayCalen/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/13 15:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayCalen/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSlurryScraperTodayCalen/list', params);
  }
}
