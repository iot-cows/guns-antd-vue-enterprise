import Request from '@/utils/request-util';

/**
 * 逻讯-液压设备api
 *
 * @author cancan
 * @date 2022/11/16 23:50
 */
export class DeviceLuoxunWaterPressureApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressure/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPressure/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPressure/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPressure/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressure/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressure/list', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressure/detailByRecordId', {"recordId" : recordId});
  }
}
