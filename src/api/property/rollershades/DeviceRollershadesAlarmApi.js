import Request from '@/utils/request-util';

/**
 * 卷帘设备告警信息api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesAlarmApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesAlarm/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesAlarm/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesAlarm/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesAlarm/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesAlarm/detail', params);
  }
}
