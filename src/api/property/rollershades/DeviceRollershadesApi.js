import Request from '@/utils/request-util';

/**
 * 卷帘设备实时数据api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershades/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershades/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershades/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershades/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershades/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceRollershades/detailByRecordId', {"recordId" : recordId});
  }
}
