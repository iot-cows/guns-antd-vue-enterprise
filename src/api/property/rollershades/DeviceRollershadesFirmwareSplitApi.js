import Request from '@/utils/request-util';

/**
 * 固件拆分后的文件api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesFirmwareSplitApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesFirmwareSplit/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesFirmwareSplit/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesFirmwareSplit/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesFirmwareSplit/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesFirmwareSplit/detail', params);
  }
}
