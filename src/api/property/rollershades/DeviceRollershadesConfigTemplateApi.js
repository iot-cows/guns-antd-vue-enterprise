import Request from '@/utils/request-util';

/**
 * 卷帘参数设置模板api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesConfigTemplateApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesConfigTemplate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesConfigTemplate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesConfigTemplate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesConfigTemplate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesConfigTemplate/detail', params);
  }
}
