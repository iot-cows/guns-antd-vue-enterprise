import Request from '@/utils/request-util';

/**
 * 固件升级进度表api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesUpgradeApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesUpgrade/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesUpgrade/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesUpgrade/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesUpgrade/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesUpgrade/detail', params);
  }
}
