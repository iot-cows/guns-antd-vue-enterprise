import Request from '@/utils/request-util';

/**
 * 卷帘参数设置api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesConfigApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesConfig/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesConfig/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesConfig/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesConfig/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesConfig/detail', params);
  }
}
