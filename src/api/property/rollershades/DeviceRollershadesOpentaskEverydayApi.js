import Request from '@/utils/request-util';

/**
 * 卷帘周期升降配置表api
 *
 * @author cancan
 * @date 2025/01/05 14:21
 */
export class DeviceRollershadesOpentaskEverydayApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesOpentaskEveryday/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2025/01/05 14:21
   */
  static add(params) {
    return Request.post('/deviceRollershadesOpentaskEveryday/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2025/01/05 14:21
   */
  static edit(params) {
    return Request.post('/deviceRollershadesOpentaskEveryday/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2025/01/05 14:21
   */
  static delete(params) {
    return Request.post('/deviceRollershadesOpentaskEveryday/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2025/01/05 14:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesOpentaskEveryday/detail', params);
  }
}
