import Request from '@/utils/request-util';

/**
 * 固件信息api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesFirmwareApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesFirmware/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesFirmware/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesFirmware/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesFirmware/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesFirmware/detail', params);
  }

  static list(params) {
    return Request.get('/deviceRollershadesFirmware/list', params);
  }
}
