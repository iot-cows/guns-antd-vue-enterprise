import Request from '@/utils/request-util';

/**
 * 卷帘参数设置历史api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesConfigLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesConfigLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesConfigLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesConfigLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesConfigLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesConfigLog/detail', params);
  }
}
