import Request from '@/utils/request-util';

/**
 * 卷帘设备历史数据api
 *
 * @author cancan
 * @date 2024/12/01 09:39
 */
export class DeviceRollershadesDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceRollershadesData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static add(params) {
    return Request.post('/deviceRollershadesData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static edit(params) {
    return Request.post('/deviceRollershadesData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static delete(params) {
    return Request.post('/deviceRollershadesData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/01 09:39
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRollershadesData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceRollershadesData/list', params);
  }
}
