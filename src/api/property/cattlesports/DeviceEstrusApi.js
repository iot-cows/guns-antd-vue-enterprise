import Request from '@/utils/request-util';

/**
 * 爬跨数据api
 *
 * @author cancan
 * @date 2023/12/02 22:21
 */
export class DeviceEstrusApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEstrus/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static add(params) {
    return Request.post('/deviceEstrus/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static edit(params) {
    return Request.post('/deviceEstrus/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static delete(params) {
    return Request.post('/deviceEstrus/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEstrus/detail', params);
  }
}
