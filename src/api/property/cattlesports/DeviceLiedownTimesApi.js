import Request from '@/utils/request-util';

/**
 * 牛躺卧按天数据统计api
 *
 * @author cancan
 * @date 2023/11/19 08:36
 */
export class DeviceLiedownTimesApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLiedownTimes/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static add(params) {
    return Request.post('/deviceLiedownTimes/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static edit(params) {
    return Request.post('/deviceLiedownTimes/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static delete(params) {
    return Request.post('/deviceLiedownTimes/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLiedownTimes/detail', params);
  }

  static list(params) {
    return Request.get('/deviceLiedownTimes/list', params);
  }
}
