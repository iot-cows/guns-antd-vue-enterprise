import Request from '@/utils/request-util';

/**
 * 定位脚环设备api
 *
 * @author cancan
 * @date 2023/09/02 11:19
 */
export class DeviceFootpositionApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceFootposition/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static add(params) {
    return Request.post('/deviceFootposition/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static edit(params) {
    return Request.post('/deviceFootposition/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static delete(params) {
    return Request.post('/deviceFootposition/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceFootposition/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceFootposition/detailByRecordId', {"recordId" : recordId});
  }
}
