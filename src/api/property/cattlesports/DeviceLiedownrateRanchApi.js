import Request from '@/utils/request-util';

/**
 * 统计牛舍按某个时间点的躺卧率api
 *
 * @author cancan
 * @date 2023/11/19 08:36
 */
export class DeviceLiedownrateRanchApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLiedownrateRanch/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static add(params) {
    return Request.post('/deviceLiedownrateRanch/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static edit(params) {
    return Request.post('/deviceLiedownrateRanch/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static delete(params) {
    return Request.post('/deviceLiedownrateRanch/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLiedownrateRanch/detail', params);
  }
}
