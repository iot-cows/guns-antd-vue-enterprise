import Request from '@/utils/request-util';

/**
 * 躺卧设备/牛躺卧数据api
 *
 * @author cancan
 * @date 2023/11/19 08:36
 */
export class DeviceLiedownApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLiedown/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static add(params) {
    return Request.post('/deviceLiedown/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static edit(params) {
    return Request.post('/deviceLiedown/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static delete(params) {
    return Request.post('/deviceLiedown/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLiedown/detail', params);
  }

  static list(params) {
    return Request.get('/deviceLiedown/list', params);
  }
}
