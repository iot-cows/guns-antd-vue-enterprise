import Request from '@/utils/request-util';

/**
 * 胃丸设备/牛胃丸数据api
 *
 * @author cancan
 * @date 2024/12/10 23:51
 */
export class DeviceStomachApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceStomach/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/10 23:51
   */
  static add(params) {
    return Request.post('/deviceStomach/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/10 23:51
   */
  static edit(params) {
    return Request.post('/deviceStomach/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/10 23:51
   */
  static delete(params) {
    return Request.post('/deviceStomach/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/10 23:51
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceStomach/detail', params);
  }

  static list(params) {
    return Request.get('/deviceStomach/list', params);
  }
}
