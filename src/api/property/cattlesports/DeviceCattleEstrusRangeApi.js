import Request from '@/utils/request-util';

/**
 * 牛的爬跨数据api
 *
 * @author cancan
 * @date 2023/12/02 22:21
 */
export class DeviceCattleEstrusRangeApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCattleEstrusRange/page', params);
  }

  static findeEtrusingCattlePage(params) {
    return Request.getAndLoadData('/deviceCattleEstrusRange/estrusingCattle/page', params);
  }

  static findeEtrusingCattleList(params) {
    return Request.getAndLoadData('/deviceCattleEstrusRange/estrusingCattle/list', params);
  }

  static statisticsEstrusCount(params) {
    return Request.getAndLoadData('/deviceCattleEstrusRange/statisticsEstrusCount', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static add(params) {
    return Request.post('/deviceCattleEstrusRange/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static edit(params) {
    return Request.post('/deviceCattleEstrusRange/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static delete(params) {
    return Request.post('/deviceCattleEstrusRange/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/12/02 22:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCattleEstrusRange/detail', params);
  }
}
