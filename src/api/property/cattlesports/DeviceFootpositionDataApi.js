import Request from '@/utils/request-util';

/**
 * 定位脚环设备历史数据api
 *
 * @author cancan
 * @date 2023/09/02 11:19
 */
export class DeviceFootpositionDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceFootpositionData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static add(params) {
    return Request.post('/deviceFootpositionData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static edit(params) {
    return Request.post('/deviceFootpositionData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static delete(params) {
    return Request.post('/deviceFootpositionData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceFootpositionData/detail', params);
  }
}
