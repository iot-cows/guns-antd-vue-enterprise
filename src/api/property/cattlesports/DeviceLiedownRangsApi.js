import Request from '@/utils/request-util';

/**
 * 牛躺卧数据api
 *
 * @author cancan
 * @date 2023/11/19 08:36
 */
export class DeviceLiedownRangsApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLiedownRangs/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static add(params) {
    return Request.post('/deviceLiedownRangs/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static edit(params) {
    return Request.post('/deviceLiedownRangs/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static delete(params) {
    return Request.post('/deviceLiedownRangs/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLiedownRangs/detail', params);
  }
}
