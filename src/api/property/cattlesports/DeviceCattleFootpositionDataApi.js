import Request from '@/utils/request-util';

/**
 * 奶牛定位脚环设备数据api
 *
 * @author cancan
 * @date 2023/09/02 11:19
 */
export class DeviceCattleFootpositionDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCattleFootpositionData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static add(params) {
    return Request.post('/deviceCattleFootpositionData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static edit(params) {
    return Request.post('/deviceCattleFootpositionData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static delete(params) {
    return Request.post('/deviceCattleFootpositionData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/09/02 11:19
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCattleFootpositionData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceCattleFootpositionData/list', params);
  }
}
