import Request from '@/utils/request-util';

/**
 * 牛躺卧按天数据统计api
 *
 * @author cancan
 * @date 2023/11/19 08:36
 */
export class DeviceLiedownHourTimesApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLiedownHourTimes/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static add(params) {
    return Request.post('/deviceLiedownHourTimes/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static edit(params) {
    return Request.post('/deviceLiedownHourTimes/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static delete(params) {
    return Request.post('/deviceLiedownHourTimes/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/11/19 08:36
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLiedownHourTimes/detail', params);
  }

  static list(params) {
    return Request.get('/deviceLiedownHourTimes/list', params);
  }
}
