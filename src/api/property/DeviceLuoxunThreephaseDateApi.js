import Request from '@/utils/request-util';

/**
 * 电日使用量统计api
 *
 * @author cancan
 * @date 2022/10/07 20:59
 */
export class DeviceLuoxunThreephaseDateApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseDate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static add(params) {
    return Request.post('/deviceLuoxunThreephaseDate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static edit(params) {
    return Request.post('/deviceLuoxunThreephaseDate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static delete(params) {
    return Request.post('/deviceLuoxunThreephaseDate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/07 20:59
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseDate/detail', params);
  }
  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseDate/list', params);
  }
}
