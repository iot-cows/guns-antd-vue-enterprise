import Request from '@/utils/request-util';

/**
 * 逻讯-气体感知设备数据api
 *
 * @author cancan
 * @date 2022/09/29 01:32
 */
export class DeviceLuoxunGasDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunGasData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static add(params) {
    return Request.post('/deviceLuoxunGasData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static edit(params) {
    return Request.post('/deviceLuoxunGasData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static delete(params) {
    return Request.post('/deviceLuoxunGasData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunGasData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunGasData/list', params);
  }
}
