import Request from '@/utils/request-util';

/**
 * 逻讯-单相电设备数据api
 *
 * @author cancan
 * @date 2022/10/04 15:24
 */
export class DeviceLuoxunSimplephaseDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static add(params) {
    return Request.post('/deviceLuoxunSimplephaseData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static edit(params) {
    return Request.post('/deviceLuoxunSimplephaseData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static delete(params) {
    return Request.post('/deviceLuoxunSimplephaseData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseData/list', params);
  }
}
