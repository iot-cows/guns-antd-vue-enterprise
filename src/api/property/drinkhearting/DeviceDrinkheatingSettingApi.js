import Request from '@/utils/request-util';

/**
 * 饮水供暖设备后台处理配置api
 *
 * @author cancan
 * @date 2024/08/17 17:43
 */
export class DeviceDrinkheatingSettingApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingSetting/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/17 17:43
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingSetting/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/17 17:43
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingSetting/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/17 17:43
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingSetting/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/17 17:43
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingSetting/detail', params);
  }
}
