import Request from '@/utils/request-util';

/**
 * 固件拆分后的文件api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingFirmwareSplitApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFirmwareSplit/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingFirmwareSplit/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingFirmwareSplit/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingFirmwareSplit/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFirmwareSplit/detail', params);
  }
}
