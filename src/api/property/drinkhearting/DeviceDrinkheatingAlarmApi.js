import Request from '@/utils/request-util';

/**
 * 饮水供暖设备告警信息api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingAlarmApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingAlarm/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingAlarm/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingAlarm/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingAlarm/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingAlarm/detail', params);
  }
}
