import Request from '@/utils/request-util';

/**
 * 每小时耗水总量api
 *
 * @author cancan
 * @date 2024/08/10 16:17
 */
export class DeviceDrinkheatingHourConsumptionApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingHourConsumption/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/10 16:17
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingHourConsumption/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/10 16:17
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingHourConsumption/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/10 16:17
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingHourConsumption/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/10 16:17
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingHourConsumption/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkheatingHourConsumption/list', params);
  }
}
