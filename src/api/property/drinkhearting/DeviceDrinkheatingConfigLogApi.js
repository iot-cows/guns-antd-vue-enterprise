import Request from '@/utils/request-util';

/**
 * 饮水供暖设备配置历史记录api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingConfigLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingConfigLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingConfigLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingConfigLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingConfigLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingConfigLog/detail', params);
  }
}
