import Request from '@/utils/request-util';

/**
 * 每天耗水总量api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingInfluentConsumptionApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingInfluentConsumption/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingInfluentConsumption/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingInfluentConsumption/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingInfluentConsumption/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingInfluentConsumption/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkheatingInfluentConsumption/list', params);
  }
}
