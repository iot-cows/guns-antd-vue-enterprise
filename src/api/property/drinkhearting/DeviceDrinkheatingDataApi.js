import Request from '@/utils/request-util';

/**
 * 饮水供暖设备历史记录api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkheatingData/list', params);
  }
}
