import Request from '@/utils/request-util';

/**
 * 每五分钟耗水总量api
 *
 * @author cancan
 * @date 2024/11/16 15:09
 */
export class DeviceDrinkheatingFiveminConsumptionApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFiveminConsumption/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/11/16 15:09
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingFiveminConsumption/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/11/16 15:09
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingFiveminConsumption/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/11/16 15:09
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingFiveminConsumption/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/11/16 15:09
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFiveminConsumption/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFiveminConsumption/list', params);
  }
}
