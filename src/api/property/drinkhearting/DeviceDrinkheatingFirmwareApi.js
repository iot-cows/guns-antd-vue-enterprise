import Request from '@/utils/request-util';

/**
 * 固件信息api
 *
 * @author cancan
 * @date 2024/08/01 23:22
 */
export class DeviceDrinkheatingFirmwareApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFirmware/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static add(params) {
    return Request.post('/deviceDrinkheatingFirmware/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static edit(params) {
    return Request.post('/deviceDrinkheatingFirmware/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static delete(params) {
    return Request.post('/deviceDrinkheatingFirmware/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/08/01 23:22
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkheatingFirmware/detail', params);
  }

  static list(params) {
    return Request.get('/deviceDrinkheatingFirmware/list', params);
  }
}
