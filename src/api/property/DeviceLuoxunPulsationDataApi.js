import Request from '@/utils/request-util';

/**
 * 逻讯-脉动设备数据api
 *
 * @author cancan
 * @date 2022/12/18 15:50
 */
export class DeviceLuoxunPulsationDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsationData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunPulsationData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunPulsationData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunPulsationData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsationData/detail', params);
  }

  static list(params) {
    return Request.get('/deviceLuoxunPulsationData/list', params);
  }
}
