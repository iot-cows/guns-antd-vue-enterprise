import Request from '@/utils/request-util';

/**
 * 公司资源-设备记录api
 *
 * @author cancan
 * @date 2022/03/05 14:43
 */
export class DeviceRecordApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceRecord/page', params);
  }


  static findPageWithSwitch(params) {
    return Request.getAndLoadData('/deviceRecord/withSwitch/page', params);
  }

  static findPageCanBindCattle(params) {
    return Request.getAndLoadData('/deviceRecord/canBindCattle/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static add(params) {
    return Request.post('/deviceRecord/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static edit(params) {
    return Request.post('/deviceRecord/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static delete(params) {
    return Request.post('/deviceRecord/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceRecord/detail', params);
  }

  static list(params) {
    return Request.get('/deviceRecord/list', params);
  }

  static  listBySwitchRecordId(params) {
    return Request.get('/deviceRecord/listBySwitchRecordId', params);
  }


  static propertyScopeTree(params) {
    return Request.get('/deviceRecord/propertyScopeTree', params);
  }

  //根据水槽设备号获取水槽网管数据
  static  watertankGatewayDetail(recordNo) {
    return Request.getAndLoadData('/deviceRecord/watertankGatewayDetail', {"sinkRecordNo" : recordNo});
  }

  static findVehiclePage(params) {
    return Request.getAndLoadData('/deviceRecord/vehicle/page', params);
  }

  static findVehicleList(params) {
    return Request.get('/deviceRecord/vehicle/list', params);
  }


  static findBasaPage(params) {
    return Request.getAndLoadData('/deviceRecord/basa/page', params);
  }

  static findBasaList(params) {
    return Request.get('/deviceRecord/basa/list', params);
  }

}
