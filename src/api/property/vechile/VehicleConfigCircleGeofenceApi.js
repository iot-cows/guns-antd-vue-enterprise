import Request from '@/utils/request-util';

/**
 * 车辆圆形电子围栏api
 *
 * @author cancan
 * @date 2024/01/06 09:46
 */
export class VehicleConfigCircleGeofenceApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static add(params) {
    return Request.post('/vehicleConfigCircleGeofence/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static edit(params) {
    return Request.post('/vehicleConfigCircleGeofence/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static delete(params) {
    return Request.post('/vehicleConfigCircleGeofence/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleConfigCircleGeofence/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/vehicleConfigCircleGeofence/list', params);
  }
}
