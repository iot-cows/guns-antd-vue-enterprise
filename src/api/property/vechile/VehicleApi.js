import Request from '@/utils/request-util';

/**
 * 车辆信息api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicle/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicle/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicle/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicle/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicle/detail', params);
  }

  static list(params) {
    return Request.get('/vehicle/list', params);
  }

  //拉取车的定位或者实时状态或数据
  static positionPointsList(params) {
    return Request.get('/vehicle/position/points/list', params);
  }

  //统计所有车的实时状态数量
  static realtimeStateCount(params) {
    return Request.get('/vehicle/realtime-state/count', params);
  }

  static bindDeviceRecord(params) {
    return Request.post('/vehicle/bind/deviceRecord', params);
  }

  static unbindDeviceRecord(params) {
    return Request.post('/vehicle/unbind/deviceRecord', params);
  }
}
