import Request from '@/utils/request-util';

/**
 * 车辆实时速度或者轨迹表api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleRealtimeSpeedApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleRealtimeSpeed/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicleRealtimeSpeed/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicleRealtimeSpeed/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicleRealtimeSpeed/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleRealtimeSpeed/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/vehicleRealtimeSpeed/list', params);
  }

  static travelList(params) {
    return Request.get('/vehicle/travel/list', params);
  }


}
