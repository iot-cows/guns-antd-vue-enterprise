import Request from '@/utils/request-util';

/**
 * 车辆多边形电子围栏api
 *
 * @author cancan
 * @date 2024/01/06 09:46
 */
export class VehicleConfigPolygonGeofenceApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static add(params) {
    return Request.post('/vehicleConfigPolygonGeofence/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static edit(params) {
    return Request.post('/vehicleConfigPolygonGeofence/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static delete(params) {
    return Request.post('/vehicleConfigPolygonGeofence/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/06 09:46
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleConfigPolygonGeofence/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/vehicleConfigPolygonGeofence/list', params);
  }
}
