import Request from '@/utils/request-util';

/**
 * 车辆多边形电子围栏-终端计算api
 *
 * @author cancan
 * @date 2024/01/13 18:43
 */
export class VehicleGeofencePolygonClientApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonClient/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static add(params) {
    return Request.post('/vehicleGeofencePolygonClient/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static edit(params) {
    return Request.post('/vehicleGeofencePolygonClient/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static delete(params) {
    return Request.post('/vehicleGeofencePolygonClient/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonClient/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonClient/list', params);
  }
}
