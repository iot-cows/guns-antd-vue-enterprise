import Request from '@/utils/request-util';

/**
 * 车辆多边形电子围栏-服务端计算api
 *
 * @author cancan
 * @date 2024/01/13 18:43
 */
export class VehicleGeofencePolygonTravelApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonTravel/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static add(params) {
    return Request.post('/vehicleGeofencePolygonTravel/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static edit(params) {
    return Request.post('/vehicleGeofencePolygonTravel/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static delete(params) {
    return Request.post('/vehicleGeofencePolygonTravel/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/13 18:43
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonTravel/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonTravel/list', params);
  }
}
