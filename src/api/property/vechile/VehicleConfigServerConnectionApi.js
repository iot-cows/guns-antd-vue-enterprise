import Request from '@/utils/request-util';

/**
 * 车辆服务器连接配置api
 *
 * @author cancan
 * @date 2023/12/24 00:50
 */
export class VehicleConfigServerConnectionApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static add(params) {
    return Request.post('/vehicleConfigServerConnection/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static edit(params) {
    return Request.post('/vehicleConfigServerConnection/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static delete(params) {
    return Request.post('/vehicleConfigServerConnection/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleConfigServerConnection/detail', params);
  }
}
