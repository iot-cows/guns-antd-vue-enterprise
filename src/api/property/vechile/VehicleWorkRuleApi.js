import Request from '@/utils/request-util';

/**
 * 按车辆设置的作业规则api
 *
 * @author cancan
 * @date 2024/01/18 23:19
 */
export class VehicleWorkRuleApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleWorkRule/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static add(params) {
    return Request.post('/vehicleWorkRule/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static edit(params) {
    return Request.post('/vehicleWorkRule/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static delete(params) {
    return Request.post('/vehicleWorkRule/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleWorkRule/detail', params);
  }

  static detailByVehicle(vehicleId) {
    return Request.getAndLoadData('/vehicleWorkRule/detailByVehicle', {"vehicleId" : vehicleId});
  }

}
