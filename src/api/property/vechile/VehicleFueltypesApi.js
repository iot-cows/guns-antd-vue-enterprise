import Request from '@/utils/request-util';

/**
 * 燃油种类api
 *
 * @author cancan
 * @date 2023/10/14 09:52
 */
export class VehicleFueltypesApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleFueltypes/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/14 09:52
   */
  static add(params) {
    return Request.post('/vehicleFueltypes/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/14 09:52
   */
  static edit(params) {
    return Request.post('/vehicleFueltypes/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/14 09:52
   */
  static delete(params) {
    return Request.post('/vehicleFueltypes/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/14 09:52
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleFueltypes/detail', params);
  }

  static list(params) {
    return Request.get('/vehicleFueltypes/list', params);
  }
}
