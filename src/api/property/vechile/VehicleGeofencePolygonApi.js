import Request from '@/utils/request-util';

/**
 * 车辆的围栏，多边形电子围栏-服务端计算api
 *
 * @author cancan
 * @date 2024/01/24 22:10
 */
export class VehicleGeofencePolygonApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygon/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/24 22:10
   */
  static add(params) {
    return Request.post('/vehicleGeofencePolygon/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/24 22:10
   */
  static edit(params) {
    return Request.post('/vehicleGeofencePolygon/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/24 22:10
   */
  static delete(params) {
    return Request.post('/vehicleGeofencePolygon/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/24 22:10
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygon/detail', params);
  }
}
