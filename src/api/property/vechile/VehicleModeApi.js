import Request from '@/utils/request-util';

/**
 * 车辆型号api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleModeApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleMode/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicleMode/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicleMode/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicleMode/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleMode/detail', params);
  }

  static list(params) {
    return Request.get('/vehicleMode/list', params);
  }
}
