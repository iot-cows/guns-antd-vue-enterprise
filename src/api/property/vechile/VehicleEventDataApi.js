import Request from '@/utils/request-util';

/**
 * 车辆事件信息api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleEventDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleEventData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicleEventData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicleEventData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicleEventData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleEventData/detail', params);
  }
}
