import Request from '@/utils/request-util';

/**
 * 车辆实时信息api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleRealtimeDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleRealtimeData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicleRealtimeData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicleRealtimeData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicleRealtimeData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleRealtimeData/detail', params);
  }
}
