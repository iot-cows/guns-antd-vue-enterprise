import Request from '@/utils/request-util';

/**
 * 牧场维度的围栏，多边形电子围栏-服务端计算api
 *
 * @author cancan
 * @date 2024/01/20 22:41
 */
export class VehicleGeofencePolygonRanchApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonRanch/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static add(params) {
    return Request.post('/vehicleGeofencePolygonRanch/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static edit(params) {
    return Request.post('/vehicleGeofencePolygonRanch/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static delete(params) {
    return Request.post('/vehicleGeofencePolygonRanch/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonRanch/detail', params);
  }
}
