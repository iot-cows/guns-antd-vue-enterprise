import Request from '@/utils/request-util';

/**
 * 车辆原型电子围栏-终端计算api
 *
 * @author cancan
 * @date 2024/01/14 08:50
 */
export class VehicleGeofenceCircleClientApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofenceCircleClient/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/14 08:50
   */
  static add(params) {
    return Request.post('/vehicleGeofenceCircleClient/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/14 08:50
   */
  static edit(params) {
    return Request.post('/vehicleGeofenceCircleClient/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/14 08:50
   */
  static delete(params) {
    return Request.post('/vehicleGeofenceCircleClient/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/14 08:50
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofenceCircleClient/detail', params);
  }
  static list(params) {
    return Request.getAndLoadData('/vehicleGeofenceCircleClient/list', params);
  }
}
