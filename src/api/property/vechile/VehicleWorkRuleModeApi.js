import Request from '@/utils/request-util';

/**
 * 按车辆类型设置的作业规则api
 *
 * @author cancan
 * @date 2024/01/18 23:19
 */
export class VehicleWorkRuleModeApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleWorkRuleMode/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static add(params) {
    return Request.post('/vehicleWorkRuleMode/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static edit(params) {
    return Request.post('/vehicleWorkRuleMode/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static delete(params) {
    return Request.post('/vehicleWorkRuleMode/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleWorkRuleMode/detail', params);
  }

  static detailByMode(modeId) {
    return Request.getAndLoadData('/vehicleWorkRuleMode/detailByMode', {"modeId" : modeId});
  }
}
