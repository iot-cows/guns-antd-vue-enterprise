import Request from '@/utils/request-util';

/**
 * 作业规则告警api
 *
 * @author cancan
 * @date 2024/01/18 23:19
 */
export class VehicleWorkRuleAlarmApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleWorkRuleAlarm/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static add(params) {
    return Request.post('/vehicleWorkRuleAlarm/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static edit(params) {
    return Request.post('/vehicleWorkRuleAlarm/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static delete(params) {
    return Request.post('/vehicleWorkRuleAlarm/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/18 23:19
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleWorkRuleAlarm/detail', params);
  }
}
