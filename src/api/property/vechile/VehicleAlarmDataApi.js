import Request from '@/utils/request-util';

/**
 * 车辆位置相关告警信息api
 *
 * @author cancan
 * @date 2023/10/03 23:15
 */
export class VehicleAlarmDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleAlarmData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static add(params) {
    return Request.post('/vehicleAlarmData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static edit(params) {
    return Request.post('/vehicleAlarmData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static delete(params) {
    return Request.post('/vehicleAlarmData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/03 23:15
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleAlarmData/detail', params);
  }
}
