import Request from '@/utils/request-util';

/**
 * 车辆设备时间配置api
 *
 * @author cancan
 * @date 2023/12/24 00:50
 */
export class VehicleConfigTimeAdjustmentApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static add(params) {
    return Request.post('/vehicleConfigTimeAdjustment/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static edit(params) {
    return Request.post('/vehicleConfigTimeAdjustment/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static delete(params) {
    return Request.post('/vehicleConfigTimeAdjustment/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/12/24 00:50
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleConfigTimeAdjustment/detail', params);
  }
}
