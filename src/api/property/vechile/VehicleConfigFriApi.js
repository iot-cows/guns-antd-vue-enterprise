import Request from '@/utils/request-util';

/**
 * 车辆速度范围告警配置api
 *
 * @author cancan
 * @date 2024/04/03 23:30
 */
export class VehicleConfigFriApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/04/03 23:30
   */
  static add(params) {
    return Request.post('/vehicleConfigFri/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/04/03 23:30
   */
  static edit(params) {
    return Request.post('/vehicleConfigFri/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/04/03 23:30
   */
  static delete(params) {
    return Request.post('/vehicleConfigFri/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/04/03 23:30
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleConfigFri/detail', params);
  }
}
