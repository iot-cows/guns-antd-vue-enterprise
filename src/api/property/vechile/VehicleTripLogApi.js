import Request from '@/utils/request-util';

/**
 * 车辆行程记录api
 *
 * @author cancan
 * @date 2023/12/12 23:30
 */
export class VehicleTripLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleTripLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/12/12 23:30
   */
  static add(params) {
    return Request.post('/vehicleTripLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/12/12 23:30
   */
  static edit(params) {
    return Request.post('/vehicleTripLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/12/12 23:30
   */
  static delete(params) {
    return Request.post('/vehicleTripLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/12/12 23:30
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleTripLog/detail', params);
  }
}
