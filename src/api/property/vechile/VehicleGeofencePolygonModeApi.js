import Request from '@/utils/request-util';

/**
 * 车辆类型维度的围栏，多边形电子围栏-服务端计算api
 *
 * @author cancan
 * @date 2024/01/20 22:41
 */
export class VehicleGeofencePolygonModeApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonMode/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static add(params) {
    return Request.post('/vehicleGeofencePolygonMode/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static edit(params) {
    return Request.post('/vehicleGeofencePolygonMode/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static delete(params) {
    return Request.post('/vehicleGeofencePolygonMode/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/01/20 22:41
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleGeofencePolygonMode/detail', params);
  }
}
