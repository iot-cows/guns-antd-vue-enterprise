import Request from '@/utils/request-util';

/**
 * 车辆行驶记录api
 *
 * @author cancan
 * @date 2023/10/14 09:54
 */
export class VehicleDriveLogApi {

  static findPage(params) {
    return Request.getAndLoadData('/vehicleDriveLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/10/14 09:54
   */
  static add(params) {
    return Request.post('/vehicleDriveLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/10/14 09:54
   */
  static edit(params) {
    return Request.post('/vehicleDriveLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/10/14 09:54
   */
  static delete(params) {
    return Request.post('/vehicleDriveLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/10/14 09:54
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleDriveLog/detail', params);
  }
}
