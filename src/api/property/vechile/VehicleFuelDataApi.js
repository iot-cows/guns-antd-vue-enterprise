import Request from '@/utils/request-util';

/**
 * 车辆油的记录api
 *
 * @author cancan
 * @date 2024/04/05 18:19
 */
export class VehicleFuelDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/vehicleFuelData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/04/05 18:19
   */
  static add(params) {
    return Request.post('/vehicleFuelData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/04/05 18:19
   */
  static edit(params) {
    return Request.post('/vehicleFuelData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/04/05 18:19
   */
  static delete(params) {
    return Request.post('/vehicleFuelData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/04/05 18:19
   */
  static detail(params) {
    return Request.getAndLoadData('/vehicleFuelData/detail', params);
  }
}
