import Request from '@/utils/request-util';

/**
 * 喷淋控制柜参数设置模板api
 *
 * @author cancan
 * @date 2024/10/03 21:26
 */
export class DeviceSprayConfigTemplateApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayConfigTemplate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static add(params) {
    return Request.post('/deviceSprayConfigTemplate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static edit(params) {
    return Request.post('/deviceSprayConfigTemplate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static delete(params) {
    return Request.post('/deviceSprayConfigTemplate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/03 21:26
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayConfigTemplate/detail', params);
  }
}
