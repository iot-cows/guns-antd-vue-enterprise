import Request from '@/utils/request-util';

/**
 * 奶牛和设备的关系api
 *
 * @author cancan
 * @date 2023/04/09 19:41
 */
export class DeviceCattleLinksApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCattleLinks/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static add(params) {
    return Request.post('/deviceCattleLinks/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static edit(params) {
    return Request.post('/deviceCattleLinks/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static delete(params) {
    return Request.post('/deviceCattleLinks/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCattleLinks/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceCattleLinks/list', params);
  }
}
