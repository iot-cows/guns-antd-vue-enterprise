import Request from '@/utils/request-util';

/**
 * 奶牛的呼吸心跳设备api
 *
 * @author cancan
 * @date 2023/04/09 19:41
 */
export class DeviceCattleHeartbeatApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeat/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static add(params) {
    return Request.post('/deviceCattleHeartbeat/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static edit(params) {
    return Request.post('/deviceCattleHeartbeat/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static delete(params) {
    return Request.post('/deviceCattleHeartbeat/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeat/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceCattleHeartbeat/detailByRecordId', {"recordId" : recordId});
  }

  static list(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeat/list', params);
  }
}
