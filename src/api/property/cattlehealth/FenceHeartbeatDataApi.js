import Request from '@/utils/request-util';

/**
 * 围栏的呼吸心跳数据api
 *
 * @author cancan
 * @date 2024/05/04 01:12
 */
export class FenceHeartbeatDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/fenceHeartbeatData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/05/04 01:12
   */
  static add(params) {
    return Request.post('/fenceHeartbeatData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/05/04 01:12
   */
  static edit(params) {
    return Request.post('/fenceHeartbeatData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/05/04 01:12
   */
  static delete(params) {
    return Request.post('/fenceHeartbeatData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/05/04 01:12
   */
  static detail(params) {
    return Request.getAndLoadData('/fenceHeartbeatData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/fenceHeartbeatData/list', params);
  }
}
