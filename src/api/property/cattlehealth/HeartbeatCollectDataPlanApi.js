import Request from '@/utils/request-util';

/**
 * 手持呼吸心跳-采集记录表api
 *
 * @author cancan
 * @date 2024/05/03 21:24
 */
export class HeartbeatCollectDataPlanApi {
  static findPage(params) {
    return Request.getAndLoadData('/heartbeatCollectDataPlan/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/05/03 21:24
   */
  static add(params) {
    return Request.post('/heartbeatCollectDataPlan/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/05/03 21:24
   */
  static edit(params) {
    return Request.post('/heartbeatCollectDataPlan/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/05/03 21:24
   */
  static delete(params) {
    return Request.post('/heartbeatCollectDataPlan/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/05/03 21:24
   */
  static detail(params) {
    return Request.getAndLoadData('/heartbeatCollectDataPlan/detail', params);
  }
}
