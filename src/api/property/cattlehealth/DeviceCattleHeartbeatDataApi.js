import Request from '@/utils/request-util';

/**
 * 奶牛的呼吸心跳历史数据api
 *
 * @author cancan
 * @date 2023/04/09 19:41
 */
export class DeviceCattleHeartbeatDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeatData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static add(params) {
    return Request.post('/deviceCattleHeartbeatData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static edit(params) {
    return Request.post('/deviceCattleHeartbeatData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static delete(params) {
    return Request.post('/deviceCattleHeartbeatData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/04/09 19:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeatData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceCattleHeartbeatData/list', params);
  }
}
