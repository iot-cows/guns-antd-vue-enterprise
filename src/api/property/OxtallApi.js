import Request from '@/utils/request-util';

/**
 * 公司资源-牛舍数据api
 *
 * @author cancan
 * @date 2022/03/02 22:49
 */
export class OxtallApi {

  static findPage(params) {
    return Request.getAndLoadData('/oxtall/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static add(params) {
    return Request.post('/oxtall/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static edit(params) {
    return Request.post('/oxtall/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static delete(params) {
    return Request.post('/oxtall/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static detail(params) {
    return Request.getAndLoadData('/oxtall/detail', params);
  }

  static list(params) {
    return Request.get('/oxtall/list', params);
  }
}
