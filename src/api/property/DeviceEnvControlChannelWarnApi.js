import Request from '@/utils/request-util';

/**
 * 环控报警历史数据api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlChannelWarnApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelWarn/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlChannelWarn/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlChannelWarn/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlChannelWarn/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelWarn/detail', params);
  }
}
