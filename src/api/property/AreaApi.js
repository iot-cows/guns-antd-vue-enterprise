import Request from '@/utils/request-util';

/**
 * 公司资源-牛舍数据-区域数据api
 *
 * @author cancan
 * @date 2022/03/02 22:49
 */
export class AreaApi {

  static findPage(params) {
    return Request.getAndLoadData('/area/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static add(params) {
    return Request.post('/area/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static edit(params) {
    return Request.post('/area/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static delete(params) {
    return Request.post('/area/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/02 22:49
   */
  static detail(params) {
    return Request.getAndLoadData('/area/detail', params);
  }

  static list(params) {
    return Request.get('/area/list', params);
  }
}
