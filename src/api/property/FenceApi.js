import Request from '@/utils/request-util';

/**
 * 公司资源-围栏数据api
 *
 * @author cancan
 * @date 2024/05/03 10:47
 */
export class FenceApi {
  static findPage(params) {
    return Request.getAndLoadData('/fence/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/05/03 10:47
   */
  static add(params) {
    return Request.post('/fence/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/05/03 10:47
   */
  static edit(params) {
    return Request.post('/fence/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/05/03 10:47
   */
  static delete(params) {
    return Request.post('/fence/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/05/03 10:47
   */
  static detail(params) {
    return Request.getAndLoadData('/fence/detail', params);
  }

  static list(params) {
    return Request.get('/fence/list', params);
  }
}
