import Request from '@/utils/request-util';

/**
 * 逻讯-单相电设备告警数据api
 *
 * @author cancan
 * @date 2022/10/04 15:24
 */
export class DeviceLuoxunSimplephaseWarnDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseWarnData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static add(params) {
    return Request.post('/deviceLuoxunSimplephaseWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static edit(params) {
    return Request.post('/deviceLuoxunSimplephaseWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static delete(params) {
    return Request.post('/deviceLuoxunSimplephaseWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephaseWarnData/detail', params);
  }
}
