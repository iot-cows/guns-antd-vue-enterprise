import Request from '@/utils/request-util';

/**
 * 喷淋控制柜告警信息api
 *
 * @author cancan
 * @date 2022/03/05 14:48
 */
export class DeviceSprayCtocabinetDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/05 14:48
   */
  static add(params) {
    return Request.post('/deviceSprayCtocabinetData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/05 14:48
   */
  static edit(params) {
    return Request.post('/deviceSprayCtocabinetData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/05 14:48
   */
  static delete(params) {
    return Request.post('/deviceSprayCtocabinetData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/05 14:48
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetData/list', params);
  }
}
