import Request from '@/utils/request-util';

/**
 * 固件拆分后的文件api
 *
 * @author cancan
 * @date 2022/04/03 18:31
 */
export class DeviceSprayFirmwareSplitApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayFirmwareSplit/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static add(params) {
    return Request.post('/deviceSprayFirmwareSplit/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static edit(params) {
    return Request.post('/deviceSprayFirmwareSplit/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static delete(params) {
    return Request.post('/deviceSprayFirmwareSplit/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayFirmwareSplit/detail', params);
  }
}
