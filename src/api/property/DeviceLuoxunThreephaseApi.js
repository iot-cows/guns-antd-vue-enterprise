import Request from '@/utils/request-util';

/**
 * 逻讯-三相电设备api
 *
 * @author cancan
 * @date 2022/10/04 15:24
 */
export class DeviceLuoxunThreephaseApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephase/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static add(params) {
    return Request.post('/deviceLuoxunThreephase/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static edit(params) {
    return Request.post('/deviceLuoxunThreephase/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static delete(params) {
    return Request.post('/deviceLuoxunThreephase/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephase/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunThreephase/detailByRecordId', {"recordId" : recordId});
  }
}
