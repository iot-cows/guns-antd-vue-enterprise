import Request from '@/utils/request-util';

/**
 * 环控设备固件信息api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlFirmwareApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlFirmware/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlFirmware/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlFirmware/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlFirmware/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlFirmware/detail', params);
  }

  static list(params) {
    return Request.get('/deviceEnvControlFirmware/list', params);
  }
}
