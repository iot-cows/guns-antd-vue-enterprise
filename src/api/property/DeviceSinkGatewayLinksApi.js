import Request from '@/utils/request-util';

/**
 * 水槽网关设备列表api
 *
 * @author cancan
 * @date 2023/01/02 09:27
 */
export class DeviceSinkGatewayLinksApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/01/02 09:27
   */
  static add(params) {
    return Request.post('/deviceSinkGatewayLinks/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/01/02 09:27
   */
  static edit(params) {
    return Request.post('/deviceSinkGatewayLinks/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/01/02 09:27
   */
  static delete(params) {
    return Request.post('/deviceSinkGatewayLinks/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/01/02 09:27
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSinkGatewayLinks/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSinkGatewayLinks/list', params);
  }
}
