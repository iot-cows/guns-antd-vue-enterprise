import Request from '@/utils/request-util';

/**
 * 逻讯-脉动设备状态数据api
 *
 * @author cancan
 * @date 2022/12/18 15:50
 */
export class DeviceLuoxunPulsationWarnDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsationWarnData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunPulsationWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunPulsationWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunPulsationWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsationWarnData/detail', params);
  }
}
