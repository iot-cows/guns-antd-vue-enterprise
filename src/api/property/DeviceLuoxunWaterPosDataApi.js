import Request from '@/utils/request-util';

/**
 * 逻讯-液位设备数据api
 *
 * @author cancan
 * @date 2022/11/16 23:50
 */
export class DeviceLuoxunWaterPosDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPosData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPosData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPosData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosData/list', params);
  }
}
