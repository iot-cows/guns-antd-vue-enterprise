import Request from '@/utils/request-util';

/**
 * 固件信息api
 *
 * @author cancan
 * @date 2022/04/03 18:31
 */
export class DeviceSprayFirmwareApi {


  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayFirmware/page', params);
  }


  /**
   * 新增
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static add(params) {
    return Request.post('/deviceSprayFirmware/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static edit(params) {
    return Request.post('/deviceSprayFirmware/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static delete(params) {
    return Request.post('/deviceSprayFirmware/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayFirmware/detail', params);
  }

  static list(params) {
    return Request.get('/deviceSprayFirmware/list', params);
  }
}
