import Request from '@/utils/request-util';

/**
 * 饮水机配置api
 *
 * @author cancan
 * @date 2023/05/20 21:41
 */
export class DeviceDrinkingStationConfigApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkingStationConfig/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static add(params) {
    return Request.post('/deviceDrinkingStationConfig/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static edit(params) {
    return Request.post('/deviceDrinkingStationConfig/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static delete(params) {
    return Request.post('/deviceDrinkingStationConfig/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkingStationConfig/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkingStationConfig/list', params);
  }


}
