import Request from '@/utils/request-util';

/**
 * 饮水机api
 *
 * @author cancan
 * @date 2023/05/20 21:41
 */
export class DeviceDrinkingStationApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkingStation/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static add(params) {
    return Request.post('/deviceDrinkingStation/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static edit(params) {
    return Request.post('/deviceDrinkingStation/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static delete(params) {
    return Request.post('/deviceDrinkingStation/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkingStation/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkingStation/list', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceDrinkingStation/detailByRecordId', {"recordId" : recordId});
  }
}
