import Request from '@/utils/request-util';

/**
 * 饮水机配置模板api
 *
 * @author cancan
 * @date 2024/10/05 21:40
 */
export class DeviceDrinkingStationConfigTemplateApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkingStationConfigTemplate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/10/05 21:40
   */
  static add(params) {
    return Request.post('/deviceDrinkingStationConfigTemplate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/10/05 21:40
   */
  static edit(params) {
    return Request.post('/deviceDrinkingStationConfigTemplate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/10/05 21:40
   */
  static delete(params) {
    return Request.post('/deviceDrinkingStationConfigTemplate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/10/05 21:40
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkingStationConfigTemplate/detail', params);
  }
}
