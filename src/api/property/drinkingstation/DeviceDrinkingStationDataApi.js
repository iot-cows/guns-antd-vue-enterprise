import Request from '@/utils/request-util';

/**
 * 饮水机数据api
 *
 * @author cancan
 * @date 2023/05/20 21:41
 */
export class DeviceDrinkingStationDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceDrinkingStationData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static add(params) {
    return Request.post('/deviceDrinkingStationData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static edit(params) {
    return Request.post('/deviceDrinkingStationData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static delete(params) {
    return Request.post('/deviceDrinkingStationData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/20 21:41
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceDrinkingStationData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceDrinkingStationData/list', params);
  }

}
