import Request from '@/utils/request-util';

/**
 * 公司资源-牧场扩展表api
 *
 * @author cancan
 * @date 2022/03/04 00:27
 */
export class RanchApi {

  static findPage(params) {
    return Request.getAndLoadData('/ranch/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/04 00:27
   */
  static add(params) {
    return Request.post('/ranch/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/04 00:27
   */
  static edit(params) {
    return Request.post('/ranch/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/04 00:27
   */
  static delete(params) {
    return Request.post('/ranch/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/04 00:27
   */
  static detail(params) {
    return Request.getAndLoadData('/ranch/detail', params);
  }

  static list(params) {
    return Request.get('/ranch/list', params);
  }
}
