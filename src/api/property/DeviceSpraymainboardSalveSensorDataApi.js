import Request from '@/utils/request-util';

/**
 * 公司资源-喷淋主板设备-从板喷淋头传感器历史数据api
 *
 * @author cancan
 * @date 2022/07/23 22:24
 */
export class DeviceSpraymainboardSalveSensorDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSpraymainboardSalveSensorData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static add(params) {
    return Request.post('/deviceSpraymainboardSalveSensorData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static edit(params) {
    return Request.post('/deviceSpraymainboardSalveSensorData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static delete(params) {
    return Request.post('/deviceSpraymainboardSalveSensorData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSpraymainboardSalveSensorData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSpraymainboardSalveSensorData/list', params);
  }
}
