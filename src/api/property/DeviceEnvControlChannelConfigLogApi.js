import Request from '@/utils/request-util';

/**
 * 环控配置历史数据api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlChannelConfigLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelConfigLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlChannelConfigLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlChannelConfigLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlChannelConfigLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelConfigLog/detail', params);
  }
}
