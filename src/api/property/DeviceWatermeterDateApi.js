import Request from '@/utils/request-util';

/**
 * 水表日使用量统计api
 *
 * @author cancan
 * @date 2022/07/02 22:21
 */
export class DeviceWatermeterDateApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceWatermeterDate/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static add(params) {
    return Request.post('/deviceWatermeterDate/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static edit(params) {
    return Request.post('/deviceWatermeterDate/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static delete(params) {
    return Request.post('/deviceWatermeterDate/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceWatermeterDate/detail', params);
  }
}
