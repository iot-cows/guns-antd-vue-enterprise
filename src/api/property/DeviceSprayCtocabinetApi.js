import Request from '@/utils/request-util';

/**
 * 喷淋控制柜api
 *
 * @author cancan
 * @date 2022/03/05 14:43
 */
export class DeviceSprayCtocabinetApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinet/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static add(params) {
    return Request.post('/deviceSprayCtocabinet/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static edit(params) {
    return Request.post('/deviceSprayCtocabinet/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static delete(params) {
    return Request.post('/deviceSprayCtocabinet/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinet/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceSprayCtocabinet/detailByRecordId', {"recordId" : recordId});
  }
}
