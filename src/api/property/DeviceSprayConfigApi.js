import Request from '@/utils/request-util';

/**
 * 喷淋控制柜参数设置api
 *
 * @author cancan
 * @date 2022/03/07 00:58
 */
export class DeviceSprayConfigApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayConfig/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static add(params) {
    return Request.post('/deviceSprayConfig/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static edit(params) {
    return Request.post('/deviceSprayConfig/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static delete(params) {
    return Request.post('/deviceSprayConfig/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayConfig/detail', params);
  }
}
