import Request from '@/utils/request-util';

/**
 * 公司资源-喷淋主板设备-从板喷淋头异常数据api
 *
 * @author cancan
 * @date 2022/07/23 22:24
 */
export class DeviceSpraymainboardSlaveErrdataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSpraymainboardSlaveErrdata/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static add(params) {
    return Request.post('/deviceSpraymainboardSlaveErrdata/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static edit(params) {
    return Request.post('/deviceSpraymainboardSlaveErrdata/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static delete(params) {
    return Request.post('/deviceSpraymainboardSlaveErrdata/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSpraymainboardSlaveErrdata/detail', params);
  }
}
