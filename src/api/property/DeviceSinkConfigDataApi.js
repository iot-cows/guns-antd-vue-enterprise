import Request from '@/utils/request-util';

/**
 * 水槽设备配置历史数据api
 *
 * @author cancan
 * @date 2022/12/25 16:20
 */
export class DeviceSinkConfigDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSinkConfigData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static add(params) {
    return Request.post('/deviceSinkConfigData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static edit(params) {
    return Request.post('/deviceSinkConfigData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static delete(params) {
    return Request.post('/deviceSinkConfigData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSinkConfigData/detail', params);
  }
}
