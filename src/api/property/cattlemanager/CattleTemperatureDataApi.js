import Request from '@/utils/request-util';

/**
 * 牛的体温数据api
 *
 * @author cancan
 * @date 2023/03/18 20:10
 */
export class CattleTemperatureDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/cattleTemperatureData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static add(params) {
    return Request.post('/cattleTemperatureData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static edit(params) {
    return Request.post('/cattleTemperatureData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static delete(params) {
    return Request.post('/cattleTemperatureData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static detail(params) {
    return Request.getAndLoadData('/cattleTemperatureData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/cattleTemperatureData/list', params);
  }
}
