import Request from '@/utils/request-util';

/**
 * 奶牛的呼吸心跳数据api
 *
 * @author cancan
 * @date 2023/05/13 00:25
 */
export class CattleHeartbeatDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/cattleHeartbeatData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/13 00:25
   */
  static add(params) {
    return Request.post('/cattleHeartbeatData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/13 00:25
   */
  static edit(params) {
    return Request.post('/cattleHeartbeatData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/13 00:25
   */
  static delete(params) {
    return Request.post('/cattleHeartbeatData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/13 00:25
   */
  static detail(params) {
    return Request.getAndLoadData('/cattleHeartbeatData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/cattleHeartbeatData/list', params);
  }
}
