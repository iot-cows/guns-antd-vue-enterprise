import Request from '@/utils/request-util';

/**
 * 牛的生命体征数据api
 *
 * @author cancan
 * @date 2023/03/18 20:10
 */
export class CattleRealtimeDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/cattleRealtimeData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static add(params) {
    return Request.post('/cattleRealtimeData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static edit(params) {
    return Request.post('/cattleRealtimeData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static delete(params) {
    return Request.post('/cattleRealtimeData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static detail(params) {
    return Request.getAndLoadData('/cattleRealtimeData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/cattleRealtimeData/list', params);
  }
}
