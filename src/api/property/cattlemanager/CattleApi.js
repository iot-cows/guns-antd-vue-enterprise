import Request from '@/utils/request-util';

/**
 * 牛api
 *
 * @author cancan
 * @date 2023/03/18 20:10
 */
export class CattleApi {

  static findPage(params) {
    return Request.getAndLoadData('/cattle/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static add(params) {
    return Request.post('/cattle/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static edit(params) {
    return Request.post('/cattle/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static delete(params) {
    return Request.post('/cattle/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static detail(params) {
    return Request.getAndLoadData('/cattle/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/cattle/list', params);
  }
}
