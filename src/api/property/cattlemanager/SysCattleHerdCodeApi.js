import Request from '@/utils/request-util';

/**
 * 总后台配置,牛群code。主要作为打通个租户牛群定义不一致的桥梁api
 *
 * @author cancan
 * @date 2023/06/17 09:28
 */
export class SysCattleHerdCodeApi {
  static findPage(params) {
    return Request.getAndLoadData('/sysCattleHerdCode/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/06/17 09:28
   */
  static add(params) {
    return Request.post('/sysCattleHerdCode/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/06/17 09:28
   */
  static edit(params) {
    return Request.post('/sysCattleHerdCode/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/06/17 09:28
   */
  static delete(params) {
    return Request.post('/sysCattleHerdCode/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/06/17 09:28
   */
  static detail(params) {
    return Request.getAndLoadData('/sysCattleHerdCode/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/sysCattleHerdCode/list', params);
  }
}
