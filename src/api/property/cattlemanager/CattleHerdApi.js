import Request from '@/utils/request-util';

/**
 * 牛群api
 *
 * @author cancan
 * @date 2023/03/18 20:10
 */
export class CattleHerdApi {

  static findPage(params) {
    return Request.getAndLoadData('/cattleHerd/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static add(params) {
    return Request.post('/cattleHerd/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static edit(params) {
    return Request.post('/cattleHerd/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static delete(params) {
    return Request.post('/cattleHerd/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/03/18 20:10
   */
  static detail(params) {
    return Request.getAndLoadData('/cattleHerd/detail', params);
  }

  static list(params) {
    return Request.get('/cattleHerd/list', params);
  }
}
