import Request from '@/utils/request-util';

/**
 * 环控历史数据api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlChannelDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlChannelData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlChannelData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlChannelData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlChannelData/detail', params);
  }
}
