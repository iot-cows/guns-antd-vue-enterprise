import Request from '@/utils/request-util';

/**
 * 环控设备数据api
 *
 * @author cancan
 * @date 2022/08/28 09:04
 */
export class DeviceEnvControlDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 09:04
   */
  static add(params) {
    return Request.post('/deviceEnvControlData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 09:04
   */
  static edit(params) {
    return Request.post('/deviceEnvControlData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 09:04
   */
  static delete(params) {
    return Request.post('/deviceEnvControlData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 09:04
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceEnvControlData/list', params);
  }
}
