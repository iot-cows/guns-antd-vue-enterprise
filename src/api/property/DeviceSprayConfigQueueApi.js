import Request from '@/utils/request-util';

/**
 * 喷淋控制柜参数设置队列api
 *
 * @author cancan
 * @date 2022/03/07 00:58
 */
export class DeviceSprayConfigQueueApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayConfigQueue/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static add(params) {
    return Request.post('/deviceSprayConfigQueue/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static edit(params) {
    return Request.post('/deviceSprayConfigQueue/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static delete(params) {
    return Request.post('/deviceSprayConfigQueue/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/07 00:58
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayConfigQueue/detail', params);
  }

  static workingDetail(params) {
    return Request.getAndLoadData('/deviceSprayConfigQueue/workingDetail', params);
  }

}
