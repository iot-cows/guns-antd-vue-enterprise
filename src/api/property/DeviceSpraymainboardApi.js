import Request from '@/utils/request-util';

/**
 * 公司资源-喷淋主板设备数据api
 *
 * @author cancan
 * @date 2022/07/23 22:24
 */
export class DeviceSpraymainboardApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSpraymainboard/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static add(params) {
    return Request.post('/deviceSpraymainboard/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static edit(params) {
    return Request.post('/deviceSpraymainboard/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static delete(params) {
    return Request.post('/deviceSpraymainboard/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/23 22:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSpraymainboard/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceSpraymainboard/detailByRecordId', {"recordId" : recordId});
  }
}
