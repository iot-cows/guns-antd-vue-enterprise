import Request from '@/utils/request-util';

/**
 * 逻讯-液位设备状态数据api
 *
 * @author cancan
 * @date 2022/11/16 23:50
 */
export class DeviceLuoxunWaterPosWarnDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosWarnData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPosWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPosWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPosWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosWarnData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosWarnData/list', params);
  }
}
