import Request from '@/utils/request-util';

/**
 * 喷淋控制柜告警信息api
 *
 * @author cancan
 * @date 2022/03/05 14:43
 */
export class DeviceSprayCtocabinetAlarmApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetAlarm/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static add(params) {
    return Request.post('/deviceSprayCtocabinetAlarm/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static edit(params) {
    return Request.post('/deviceSprayCtocabinetAlarm/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static delete(params) {
    return Request.post('/deviceSprayCtocabinetAlarm/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/05 14:43
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetAlarm/detail', params);
  }
}
