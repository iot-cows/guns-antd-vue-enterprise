import Request from '@/utils/request-util';

/**
 * 水槽设备点动和手动开关api
 *
 * @author cancan
 * @date 2023/01/15 16:53
 */
export class DeviceSinkSwitchDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSinkSwitchData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/01/15 16:53
   */
  static add(params) {
    return Request.post('/deviceSinkSwitchData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/01/15 16:53
   */
  static edit(params) {
    return Request.post('/deviceSinkSwitchData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/01/15 16:53
   */
  static delete(params) {
    return Request.post('/deviceSinkSwitchData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/01/15 16:53
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSinkSwitchData/detail', params);
  }
}
