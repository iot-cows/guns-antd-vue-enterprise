import Request from '@/utils/request-util';

/**
 * 逻讯-液压设备状态数据api
 *
 * @author cancan
 * @date 2022/11/16 23:50
 */
export class DeviceLuoxunWaterPressureWarnDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressureWarnData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPressureWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPressureWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPressureWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressureWarnData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPressureWarnData/list', params);
  }
}
