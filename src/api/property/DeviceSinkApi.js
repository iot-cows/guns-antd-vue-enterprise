import Request from '@/utils/request-util';

/**
 * 水槽设备api
 *
 * @author cancan
 * @date 2022/12/25 16:20
 */
export class DeviceSinkApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSink/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static add(params) {
    return Request.post('/deviceSink/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static edit(params) {
    return Request.post('/deviceSink/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static delete(params) {
    return Request.post('/deviceSink/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSink/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceSink/detailByRecordId', {"recordId" : recordId});
  }
}
