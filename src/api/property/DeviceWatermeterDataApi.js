import Request from '@/utils/request-util';

/**
 * 水表抄表数据api
 *
 * @author cancan
 * @date 2022/07/02 22:21
 */
export class DeviceWatermeterDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceWatermeterData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static add(params) {
    return Request.post('/deviceWatermeterData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static edit(params) {
    return Request.post('/deviceWatermeterData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static delete(params) {
    return Request.post('/deviceWatermeterData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/07/02 22:21
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceWatermeterData/detail', params);
  }
}
