import Request from '@/utils/request-util';

/**
 * 喷淋控制柜设备实时状态信息api
 *
 * @author cancan
 * @date 2022/03/29 22:49
 */
export class DeviceSprayCtocabinetStateApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetState/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/29 22:49
   */
  static add(params) {
    return Request.post('/deviceSprayCtocabinetState/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/29 22:49
   */
  static edit(params) {
    return Request.post('/deviceSprayCtocabinetState/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/29 22:49
   */
  static delete(params) {
    return Request.post('/deviceSprayCtocabinetState/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/29 22:49
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayCtocabinetState/detail', params);
  }
}
