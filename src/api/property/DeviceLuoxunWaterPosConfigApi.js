import Request from '@/utils/request-util';

/**
 * 水位下发配置api
 *
 * @author cancan
 * @date 2023/06/04 10:09
 */
export class DeviceLuoxunWaterPosConfigApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosConfig/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/06/04 10:09
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPosConfig/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/06/04 10:09
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPosConfig/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/06/04 10:09
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPosConfig/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/06/04 10:09
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPosConfig/detail', params);
  }
}
