import Request from '@/utils/request-util';

/**
 * 公司资源-设备类型信息api
 *
 * @author cancan
 * @date 2022/03/04 21:15
 */
export class DeviceApi {

  static findPage(params) {
    return Request.getAndLoadData('/device/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static add(params) {
    return Request.post('/device/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static edit(params) {
    return Request.post('/device/edit', params);
  }

  static tree(params) {
    return Request.get('/device/tree', params);
  }

  static list(params) {
    return Request.get('/device/list', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static delete(params) {
    return Request.post('/device/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/03/04 21:15
   */
  static detail(params) {
    return Request.getAndLoadData('/device/detail', params);
  }
}
