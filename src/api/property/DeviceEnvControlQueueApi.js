import Request from '@/utils/request-util';

/**
 * 环控参数设置队列api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlQueueApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlQueue/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlQueue/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlQueue/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlQueue/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlQueue/detail', params);
  }

  static workingDetail(params) {
    return Request.getAndLoadData('/deviceEnvControlQueue/workingDetail', params);
  }
}
