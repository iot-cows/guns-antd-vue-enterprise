import Request from '@/utils/request-util';

/**
 * 逻讯-脉动设备api
 *
 * @author cancan
 * @date 2022/12/18 15:50
 */
export class DeviceLuoxunPulsationApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsation/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunPulsation/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunPulsation/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunPulsation/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/18 15:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunPulsation/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunPulsation/detailByRecordId', {"recordId" : recordId});
  }
}
