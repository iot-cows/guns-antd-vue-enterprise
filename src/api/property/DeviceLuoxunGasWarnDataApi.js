import Request from '@/utils/request-util';

/**
 * 逻讯-气体感知设备状态数据api
 *
 * @author cancan
 * @date 2022/09/29 01:32
 */
export class DeviceLuoxunGasWarnDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunGasWarnData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static add(params) {
    return Request.post('/deviceLuoxunGasWarnData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static edit(params) {
    return Request.post('/deviceLuoxunGasWarnData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static delete(params) {
    return Request.post('/deviceLuoxunGasWarnData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/09/29 01:32
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunGasWarnData/detail', params);
  }
}
