import Request from '@/utils/request-util';

/**
 * 固件升级进度表api
 *
 * @author cancan
 * @date 2022/04/03 18:31
 */
export class DeviceSprayUpgradeApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSprayUpgrade/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static add(params) {
    return Request.post('/deviceSprayUpgrade/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static edit(params) {
    return Request.post('/deviceSprayUpgrade/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static delete(params) {
    return Request.post('/deviceSprayUpgrade/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/04/03 18:31
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSprayUpgrade/detail', params);
  }
}
