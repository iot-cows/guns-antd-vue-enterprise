import Request from '@/utils/request-util';

/**
 * 逻讯-液位设备api
 *
 * @author cancan
 * @date 2022/11/16 23:50
 */
export class DeviceLuoxunWaterPosApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPos/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static add(params) {
    return Request.post('/deviceLuoxunWaterPos/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static edit(params) {
    return Request.post('/deviceLuoxunWaterPos/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static delete(params) {
    return Request.post('/deviceLuoxunWaterPos/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/11/16 23:50
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPos/detail', params);
  }
  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunWaterPos/list', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunWaterPos/detailByRecordId', {"recordId" : recordId});
  }
}
