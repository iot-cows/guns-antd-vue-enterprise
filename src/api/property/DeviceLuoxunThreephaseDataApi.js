import Request from '@/utils/request-util';

/**
 * 逻讯-三相电设备数据api
 *
 * @author cancan
 * @date 2022/10/04 15:24
 */
export class DeviceLuoxunThreephaseDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static add(params) {
    return Request.post('/deviceLuoxunThreephaseData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static edit(params) {
    return Request.post('/deviceLuoxunThreephaseData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static delete(params) {
    return Request.post('/deviceLuoxunThreephaseData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceLuoxunThreephaseData/list', params);
  }
}
