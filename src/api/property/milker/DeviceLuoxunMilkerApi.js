import Request from '@/utils/request-util';

/**
 * 逻讯-挤奶机api
 *
 * @author cancan
 * @date 2023/02/05 13:55
 */
export class DeviceLuoxunMilkerApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunMilker/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static add(params) {
    return Request.post('/deviceLuoxunMilker/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static edit(params) {
    return Request.post('/deviceLuoxunMilker/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static delete(params) {
    return Request.post('/deviceLuoxunMilker/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunMilker/detail', params);
  }
}
