import Request from '@/utils/request-util';

/**
 * 逻讯-挤奶机位置关系api
 *
 * @author cancan
 * @date 2023/02/05 13:55
 */
export class DeviceLuoxunMilkerPositionApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunMilkerPosition/page', params);
  }

  static findPositionsOfMilkerPage(params) {
    return Request.getAndLoadData('/deviceLuoxunMilkerPosition/positionsOfMilkerPage', params);
  }


  /**
   * 新增
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static add(params) {
    return Request.post('/deviceLuoxunMilkerPosition/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static edit(params) {
    return Request.post('/deviceLuoxunMilkerPosition/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static delete(params) {
    return Request.post('/deviceLuoxunMilkerPosition/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/02/05 13:55
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunMilkerPosition/detail', params);
  }
}
