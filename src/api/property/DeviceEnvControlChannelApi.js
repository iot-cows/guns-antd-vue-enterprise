import Request from '@/utils/request-util';

/**
 * 环控设备传感器api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlChannelApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlChannel/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlChannel/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlChannel/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlChannel/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlChannel/detail', params);
  }
}
