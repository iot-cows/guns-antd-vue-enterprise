import Request from '@/utils/request-util';

/**
 * 环控设备固件升级进度表api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlUpgradeApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlUpgrade/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlUpgrade/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlUpgrade/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlUpgrade/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlUpgrade/detail', params);
  }
}
