import Request from '@/utils/request-util';

/**
 * 环控设备固件拆分后的文件api
 *
 * @author cancan
 * @date 2022/08/28 08:12
 */
export class DeviceEnvControlFirmwareSplitApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceEnvControlFirmwareSplit/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static add(params) {
    return Request.post('/deviceEnvControlFirmwareSplit/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static edit(params) {
    return Request.post('/deviceEnvControlFirmwareSplit/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static delete(params) {
    return Request.post('/deviceEnvControlFirmwareSplit/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/08/28 08:12
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceEnvControlFirmwareSplit/detail', params);
  }
}
