import Request from '@/utils/request-util';

/**
 * 每次上报的统计量api
 *
 * @author cancan
 * @date 2023/06/10 17:35
 */
export class DeviceWatermeterSubApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceWatermeterSub/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/06/10 17:35
   */
  static add(params) {
    return Request.post('/deviceWatermeterSub/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/06/10 17:35
   */
  static edit(params) {
    return Request.post('/deviceWatermeterSub/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/06/10 17:35
   */
  static delete(params) {
    return Request.post('/deviceWatermeterSub/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/06/10 17:35
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceWatermeterSub/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceWatermeterSub/list', params);
  }
}
