import Request from '@/utils/request-util';

/**
 * 巴杀设备api
 *
 * @author cancan
 * @date 2024/03/10 22:34
 */
export class BasaApi {

  static findPage(params) {
    return Request.getAndLoadData('/basa/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static add(params) {
    return Request.post('/basa/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static edit(params) {
    return Request.post('/basa/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static delete(params) {
    return Request.post('/basa/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static detail(params) {
    return Request.getAndLoadData('/basa/detail', params);
  }

  static iotAction(params) {
    return Request.getAndLoadData('/basa/iot/action', params);
  }

  static bindDeviceRecord(params) {
    return Request.post('/basa/bind/deviceRecord', params);
  }

  static unbindDeviceRecord(params) {
    return Request.post('/basa/unbind/deviceRecord', params);
  }
}
