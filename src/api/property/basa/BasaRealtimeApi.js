import Request from '@/utils/request-util';

/**
 * 巴杀实时状态数据api
 *
 * @author cancan
 * @date 2024/03/10 22:34
 */
export class BasaRealtimeApi {

  static findPage(params) {
    return Request.getAndLoadData('/basaRealtime/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static add(params) {
    return Request.post('/basaRealtime/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static edit(params) {
    return Request.post('/basaRealtime/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static delete(params) {
    return Request.post('/basaRealtime/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static detail(params) {
    return Request.getAndLoadData('/basaRealtime/detail', params);
  }
}
