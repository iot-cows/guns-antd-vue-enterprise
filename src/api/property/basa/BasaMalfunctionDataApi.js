import Request from '@/utils/request-util';

/**
 * 巴杀故障数据api
 *
 * @author cancan
 * @date 2024/03/10 22:34
 */
export class BasaMalfunctionDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/basaMalfunctionData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static add(params) {
    return Request.post('/basaMalfunctionData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static edit(params) {
    return Request.post('/basaMalfunctionData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static delete(params) {
    return Request.post('/basaMalfunctionData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static detail(params) {
    return Request.getAndLoadData('/basaMalfunctionData/detail', params);
  }
}
