import Request from '@/utils/request-util';

/**
 * 巴杀组件状态数据api
 *
 * @author cancan
 * @date 2024/03/10 22:34
 */
export class BasaComponetsDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/basaComponetsData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static add(params) {
    return Request.post('/basaComponetsData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static edit(params) {
    return Request.post('/basaComponetsData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static delete(params) {
    return Request.post('/basaComponetsData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/03/10 22:34
   */
  static detail(params) {
    return Request.getAndLoadData('/basaComponetsData/detail', params);
  }
}
