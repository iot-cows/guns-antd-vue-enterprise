import Request from '@/utils/request-util';

/**
 * 巴杀的原始数据api
 *
 * @author cancan
 * @date 2024/07/02 20:04
 */
export class BasaSourceDataApi {
  static findPage(params) {
    return Request.getAndLoadData('/basaSourceData/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/07/02 20:04
   */
  static add(params) {
    return Request.post('/basaSourceData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/07/02 20:04
   */
  static edit(params) {
    return Request.post('/basaSourceData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/07/02 20:04
   */
  static delete(params) {
    return Request.post('/basaSourceData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/07/02 20:04
   */
  static detail(params) {
    return Request.getAndLoadData('/basaSourceData/detail', params);
  }
}
