import Request from '@/utils/request-util';

/**
 * 逻讯-单相电设备api
 *
 * @author cancan
 * @date 2022/10/04 15:24
 */
export class DeviceLuoxunSimplephaseApi {
  static findPage(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephase/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static add(params) {
    return Request.post('/deviceLuoxunSimplephase/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static edit(params) {
    return Request.post('/deviceLuoxunSimplephase/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static delete(params) {
    return Request.post('/deviceLuoxunSimplephase/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/04 15:24
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceLuoxunSimplephase/detail', params);
  }

  static detailByRecord(recordId) {
    return Request.getAndLoadData('/deviceLuoxunSimplephase/detailByRecordId', {"recordId" : recordId});
  }
}
