import Request from '@/utils/request-util';

/**
 * 水槽设备数据api
 *
 * @author cancan
 * @date 2022/12/25 16:20
 */
export class DeviceSinkDataApi {

  static findPage(params) {
    return Request.getAndLoadData('/deviceSinkData/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static add(params) {
    return Request.post('/deviceSinkData/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static edit(params) {
    return Request.post('/deviceSinkData/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static delete(params) {
    return Request.post('/deviceSinkData/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/12/25 16:20
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSinkData/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/deviceSinkData/list', params);
  }
}
