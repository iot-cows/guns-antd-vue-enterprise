import Request from '@/utils/request-util';

/**
 * 设备开关控制api
 *
 * @author cancan
 * @date 2022/10/29 16:31
 */
export class DeviceSwitchMappingApi {
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/10/29 16:31
   */
  static add(params) {
    return Request.post('/deviceSwitchMapping/add', params);
  }

  static unbind(params) {
    return Request.post('/deviceSwitchMapping/unbind', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/10/29 16:31
   */
  static edit(params) {
    return Request.post('/deviceSwitchMapping/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/10/29 16:31
   */
  static delete(params) {
    return Request.post('/deviceSwitchMapping/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/10/29 16:31
   */
  static detail(params) {
    return Request.getAndLoadData('/deviceSwitchMapping/detail', params);
  }
}
