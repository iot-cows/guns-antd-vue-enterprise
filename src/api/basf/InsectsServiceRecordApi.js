import Request from '@/utils/request-util';

/**
 * 服务记录api
 *
 * @author cancan
 * @date 2024/11/18 23:25
 */
export class InsectsServiceRecordApi {
  static findPage(params) {
    return Request.getAndLoadData('/insectsServiceRecord/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/11/18 23:25
   */
  static add(params) {
    return Request.post('/insectsServiceRecord/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/11/18 23:25
   */
  static edit(params) {
    return Request.post('/insectsServiceRecord/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/11/18 23:25
   */
  static delete(params) {
    return Request.post('/insectsServiceRecord/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/11/18 23:25
   */
  static detail(params) {
    return Request.getAndLoadData('/insectsServiceRecord/detail', params);
  }
}
