import Request from '@/utils/request-util';

/**
 * 数据下载任务api
 *
 * @author cancan
 * @date 2023/06/17 23:28
 */
export class SysDatadownLogApi {

  static findPage(params) {
    return Request.getAndLoadData('/sysDatadownLog/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/06/17 23:28
   */
  static add(params) {
    return Request.post('/sysDatadownLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/06/17 23:28
   */
  static edit(params) {
    return Request.post('/sysDatadownLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/06/17 23:28
   */
  static delete(params) {
    return Request.post('/sysDatadownLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/06/17 23:28
   */
  static detail(params) {
    return Request.getAndLoadData('/sysDatadownLog/detail', params);
  }
}
