import Request from '@/utils/request-util';

/**
 * 告警规则/条件api
 *
 * @author cancan
 * @date 2023/05/28 11:35
 */
export class MonitorRulesApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorRules/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static add(params) {
    return Request.post('/monitorRules/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static edit(params) {
    return Request.post('/monitorRules/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static delete(params) {
    return Request.post('/monitorRules/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorRules/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/monitorRules/list', params);
  }
}
