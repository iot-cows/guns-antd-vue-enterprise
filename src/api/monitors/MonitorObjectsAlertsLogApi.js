import Request from '@/utils/request-util';

/**
 * 告警主题历史表api
 *
 * @author cancan
 * @date 2023/06/11 14:33
 */
export class MonitorObjectsAlertsLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorObjectsAlertsLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/06/11 14:33
   */
  static add(params) {
    return Request.post('/monitorObjectsAlertsLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/06/11 14:33
   */
  static edit(params) {
    return Request.post('/monitorObjectsAlertsLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/06/11 14:33
   */
  static delete(params) {
    return Request.post('/monitorObjectsAlertsLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/06/11 14:33
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorObjectsAlertsLog/detail', params);
  }
}
