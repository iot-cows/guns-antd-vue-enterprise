import Request from '@/utils/request-util';

/**
 * 事件告警通知方式api
 *
 * @author cancan
 * @date 2024/12/16 22:35
 */
export class MonitorEventStrategyNoticeApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEventStrategyNotice/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/16 22:35
   */
  static add(params) {
    return Request.post('/monitorEventStrategyNotice/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/16 22:35
   */
  static edit(params) {
    return Request.post('/monitorEventStrategyNotice/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/16 22:35
   */
  static delete(params) {
    return Request.post('/monitorEventStrategyNotice/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/16 22:35
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEventStrategyNotice/detail', params);
  }

  static objectsContactsList(strategyId)
  {
    return Request.getAndLoadData('/monitorEventStrategyNotice/eventStrategyContacts', {"strategyId" : strategyId});
  }

  static editNotices(params) {
    return Request.post('/monitorEventStrategyNotice/addEventStrategyNotices', params);
  }
}
