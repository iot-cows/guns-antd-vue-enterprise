import Request from '@/utils/request-util';

/**
 * 订阅事件主题api
 *
 * @author cancan
 * @date 2024/12/15 16:16
 */
export class MonitorEventSubscribeStrategyApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEventSubscribeStrategy/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static add(params) {
    return Request.post('/monitorEventSubscribeStrategy/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static edit(params) {
    return Request.post('/monitorEventSubscribeStrategy/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static delete(params) {
    return Request.post('/monitorEventSubscribeStrategy/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEventSubscribeStrategy/detail', params);
  }
}
