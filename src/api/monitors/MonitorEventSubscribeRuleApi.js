import Request from '@/utils/request-util';

/**
 * 订阅事件规则api
 *
 * @author cancan
 * @date 2024/12/15 16:16
 */
export class MonitorEventSubscribeRuleApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEventSubscribeRule/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static add(params) {
    return Request.post('/monitorEventSubscribeRule/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static edit(params) {
    return Request.post('/monitorEventSubscribeRule/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static delete(params) {
    return Request.post('/monitorEventSubscribeRule/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEventSubscribeRule/detail', params);
  }
}
