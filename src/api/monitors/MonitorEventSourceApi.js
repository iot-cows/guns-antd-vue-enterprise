import Request from '@/utils/request-util';

/**
 * 资源api
 *
 * @author cancan
 * @date 2024/12/15 16:16
 */
export class MonitorEventSourceApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEventSource/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static add(params) {
    return Request.post('/monitorEventSource/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static edit(params) {
    return Request.post('/monitorEventSource/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static delete(params) {
    return Request.post('/monitorEventSource/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEventSource/detail', params);
  }

  static list(params) {
    return Request.get('/monitorEventSource/list', params);
  }
}
