import Request from '@/utils/request-util';

/**
 * 事件表api
 *
 * @author cancan
 * @date 2024/12/15 16:16
 */
export class MonitorEventApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEvent/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static add(params) {
    return Request.post('/monitorEvent/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static edit(params) {
    return Request.post('/monitorEvent/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static delete(params) {
    return Request.post('/monitorEvent/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEvent/detail', params);
  }

  static list(params) {
    return Request.get('/monitorEvent/list', params);
  }
}
