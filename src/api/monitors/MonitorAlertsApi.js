import Request from '@/utils/request-util';

/**
 * 告警统计表api
 *
 * @author cancan
 * @date 2023/05/28 11:35
 */
export class MonitorAlertsApi {

  static findPage(params) {
    return Request.getAndLoadData('/monitorAlerts/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static add(params) {
    return Request.post('/monitorAlerts/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static edit(params) {
    return Request.post('/monitorAlerts/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static delete(params) {
    return Request.post('/monitorAlerts/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorAlerts/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/monitorAlerts/list', params);
  }
}
