import Request from '@/utils/request-util';

/**
 * 告警通知方式api
 *
 * @author cancan
 * @date 2023/08/12 22:47
 */
export class MonitorObjectsNoticeApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorObjectsNotice/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static add(params) {
    return Request.post('/monitorObjectsNotice/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static edit(params) {
    return Request.post('/monitorObjectsNotice/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static delete(params) {
    return Request.post('/monitorObjectsNotice/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorObjectsNotice/detail', params);
  }

  static objectsContactsList(monitorObjectsId)
  {
    return Request.getAndLoadData('/monitorObjectsNotice/ObjectsContacts', {"monitorObjectsId" : monitorObjectsId});
  }

  static editNotices(params) {
    return Request.post('/monitorObjectsNotice/addObjNotices', params);
  }

}
