import Request from '@/utils/request-util';

/**
 * 联系人api
 *
 * @author cancan
 * @date 2023/08/12 22:47
 */
export class MonitorContactsApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorContacts/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static add(params) {
    return Request.post('/monitorContacts/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static edit(params) {
    return Request.post('/monitorContacts/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static delete(params) {
    return Request.post('/monitorContacts/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/08/12 22:47
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorContacts/detail', params);
  }

  static list(params) {
    return Request.get('/monitorContacts/list', params);
  }
}
