import Request from '@/utils/request-util';

/**
 * 告警维度名称映射api
 *
 * @author cancan
 * @date 2023/05/28 11:35
 */
export class MonitorDimensionApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorDimension/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static add(params) {
    return Request.post('/monitorDimension/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static edit(params) {
    return Request.post('/monitorDimension/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static delete(params) {
    return Request.post('/monitorDimension/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorDimension/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/monitorDimension/list', params);
  }
}
