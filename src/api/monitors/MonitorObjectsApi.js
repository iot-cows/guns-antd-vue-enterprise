import Request from '@/utils/request-util';

/**
 * 监控对象名称api
 *
 * @author cancan
 * @date 2023/05/28 11:35
 */
export class MonitorObjectsApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorObjects/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static add(params) {
    return Request.post('/monitorObjects/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static edit(params) {
    return Request.post('/monitorObjects/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static delete(params) {
    return Request.post('/monitorObjects/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/28 11:35
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorObjects/detail', params);
  }

  static list(params) {
    return Request.getAndLoadData('/monitorObjects/list', params);
  }
}
