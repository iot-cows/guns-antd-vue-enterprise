import Request from '@/utils/request-util';

/**
 * 事件告警历史表api
 *
 * @author cancan
 * @date 2024/12/15 16:16
 */
export class MonitorEventTriggerLogApi {
  static findPage(params) {
    return Request.getAndLoadData('/monitorEventTriggerLog/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static add(params) {
    return Request.post('/monitorEventTriggerLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static edit(params) {
    return Request.post('/monitorEventTriggerLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static delete(params) {
    return Request.post('/monitorEventTriggerLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2024/12/15 16:16
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorEventTriggerLog/detail', params);
  }
}
