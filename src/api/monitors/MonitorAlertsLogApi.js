import Request from '@/utils/request-util';

/**
 * 告警历史表api
 *
 * @author cancan
 * @date 2023/05/29 22:23
 */
export class MonitorAlertsLogApi {

  static findCowAlertLogPage(params) {
    return Request.getAndLoadData('/monitorAlertsLog/cowDiseases/page', params);
  }

  static findPage(params) {
    return Request.getAndLoadData('/monitorAlertsLog/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2023/05/29 22:23
   */
  static add(params) {
    return Request.post('/monitorAlertsLog/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2023/05/29 22:23
   */
  static edit(params) {
    return Request.post('/monitorAlertsLog/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2023/05/29 22:23
   */
  static delete(params) {
    return Request.post('/monitorAlertsLog/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2023/05/29 22:23
   */
  static detail(params) {
    return Request.getAndLoadData('/monitorAlertsLog/detail', params);
  }
}
