import Request from '@/utils/request-util';

/**
 * 租户和设备的映射api
 *
 * @author cancan
 * @date 2022/04/30 19:10
 */
export class DevicemappingApi {

  static findPage(params) {
    return Request.getAndLoadData('/devicemapping/page', params);
  }
  /**
   * 新增
   *
   * @author cancan
   * @date 2022/04/30 19:10
   */
  static add(params) {
    return Request.post('/devicemapping/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/04/30 19:10
   */
  static edit(params) {
    return Request.post('/devicemapping/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/04/30 19:10
   */
  static delete(params) {
    return Request.post('/devicemapping/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/04/30 19:10
   */
  static detail(params) {
    return Request.getAndLoadData('/devicemapping/detail', params);
  }
}
