import Request from '@/utils/request-util';

/**
 * 租户映射api
 *
 * @author cancan
 * @date 2022/05/02 22:07
 */
export class UsermappingApi {

  static findPage(params) {
    return Request.getAndLoadData('/tenantUsermapping/page', params);
  }

  /**
   * 新增
   *
   * @author cancan
   * @date 2022/05/02 22:07
   */
  static add(params) {
    return Request.post('/tenantUsermapping/add', params);
  }

  /**
   * 修改
   *
   * @author cancan
   * @date 2022/05/02 22:07
   */
  static edit(params) {
    return Request.post('/tenantUsermapping/edit', params);
  }

  /**
   * 删除
   *
   * @author cancan
   * @date 2022/05/02 22:07
   */
  static delete(params) {
    return Request.post('/tenantUsermapping/delete', params);
  }

  /**
   * 详情
   *
   * @author cancan
   * @date 2022/05/02 22:07
   */
  static detail(params) {
    return Request.getAndLoadData('/tenantUsermapping/detail', params);
  }
}
