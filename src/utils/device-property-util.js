/**
 * 环控设备传感器类型
 * @param nodes
 * @returns {*}
 */
export function getEnvControlType(channelType) {
  if (channelType == 1) {
    return '温度';
  } else if (channelType == 2) {
    return '湿度';
  } else if (channelType == 3) {
    return 'CO2';
  } else if (channelType == 4) {
    return 'NH3';
  } else if (channelType == 5) {
    return 'H2S';
  } else if (channelType == 6) {
    return '光照';
  } else if (channelType == 7) {
    return '气压';
  } else if (channelType == 8) {
    return 'GPS';
  } else if (channelType == 9) {
    return '土壤温度';
  } else if (channelType == 10) {
    return '土壤湿度';
  } else if (channelType == 11) {
    return '土壤电导率';
  } else if (channelType == 12) {
    return '土壤PH值';
  } else if (channelType == 13) {
    return 'O3';
  } else if (channelType == 14) {
    return 'O2';
  } else if (channelType == 15) {
    return '氮';
  } else if (channelType == 16) {
    return '磷';
  } else if (channelType == 17) {
    return '钾';
  } else if (channelType == 18) {
    return 'Pm2.5';
  } else if (channelType == 19) {
    return 'PM10';
  } else if (channelType == 20) {
    return 'CH4';
  } else if (channelType == 21) {
    return '风向';
  } else if (channelType == 22) {
    return '风速';
  } else if (channelType == 23) {
    return '雨量';
  } else if (channelType == 24) {
    return '液位';
  } else if (channelType == 0) {
    return 'AI0';
  }
}
