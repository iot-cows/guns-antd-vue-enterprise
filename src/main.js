import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import permission from './utils/permission';
import i18n from './i18n';
import './styles/index.less';
import * as antIcons from '@ant-design/icons-vue';

const app = createApp(App);

app.use(router);
app.use(store);
app.use(permission);
app.use(i18n);

app.mount('#app');

// 注册图标组件到全局
Object.keys(antIcons).forEach(key => {
  app.component(key, antIcons[key]);
});
app.config.globalProperties.$antIcons = antIcons;
