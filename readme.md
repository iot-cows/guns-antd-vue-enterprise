# 需要下载node 16

# 拉代码注意，最好git禁用掉自动切换CRLF
git config --global core.autocrlf false

# 先安装依赖
yarn

# 访问国外网站比较慢的用户可以在后面加--registry
yarn --registry=https://registry.npmmirror.com

# 依赖安装好后运行项目
yarn run dev

# 运行时重新生成缓存文件
yarn run dev --force

# 安装到dev环境包
yarn add -D xxx

# 安装包
yarn add xxx

# 安装qs
yarn add qs

