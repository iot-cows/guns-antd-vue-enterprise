import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import legacy from '@vitejs/plugin-legacy';
import ViteCompression from 'vite-plugin-compression';
import ViteComponents from 'unplugin-vue-components/vite';
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers';
import { EleAdminResolver, styleDeps } from 'ele-admin-pro/lib/utils/resolvers';
import { DynamicAntdLess } from 'ele-admin-pro/lib/utils/dynamic-theme';
import { resolve } from 'path';

export default defineConfig(({ mode, command }) => {
  console.log(command);
  const isBuild = command === 'build';
  const env = loadEnv(mode, process.cwd());
  console.log(env);
  return {
    server: {
      port: 8000,
      proxy: {
        '/api': {
          target: env.VITE_API_URL,
          changeOrigin: true,
          ws: true,
          rewrite: path => path.replace(/^\/api/, '')
        }
      }
    },
    resolve: {
      alias: {
        '@/': resolve(__dirname, 'src') + '/',
        'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js'
      }
    },
    plugins: [
      vue(),
      // 组件按需引入
      ViteComponents({
        dts: false,
        resolvers: [
          AntDesignVueResolver({
            importStyle: 'less'
          }),
          EleAdminResolver({
            importStyle: true
          })
        ]
      }),
      // gzip 压缩
      ViteCompression({
        disable: !isBuild,
        threshold: 10240,
        algorithm: 'gzip',
        ext: '.gz'
      }),
      // 兼容低版本浏览器
      legacy({
        targets: ['Chrome 63'],
        modernPolyfills: true
      })
    ],
    css: {
      preprocessorOptions: {
        less: {
          javascriptEnabled: true,
          plugins: [new DynamicAntdLess()]
        }
      }
    },
    optimizeDeps: {
      include: [
        ...styleDeps,
        'ant-design-vue',
        'ant-design-vue/es',
        'ele-admin-pro',
        'ele-admin-pro/es',
        '@ant-design/icons-vue',
        'vuedraggable',
        'dayjs',
        'echarts/core',
        'echarts/charts',
        'echarts/renderers',
        'echarts/components',
        'vue-echarts',
        'echarts-wordcloud',
        'xlsx'
      ]
    },
    build: {
      target: 'chrome63',
      rollupOptions: {
        output: {
          manualChunks: id => {
            // 使用函数形式时，每个解析的模块 id 都会传递给函数。
            // 如果返回字符串，则模块及其所有依赖项将添加到具有给定名称的手动块中。
            // 例如，这将创建一个vendor包含所有依赖项的块node_modules：
            console.log(id);
            if (id.includes("/views/property/device/")) {
              if (id.includes("/property/device/envcontrol/")) {
                return 'views_property_device_envcontrol'
              } else if (id.includes("/property/device/luoxungas/") || id.includes("/property/device/luoxunsimplephase/")
                || id.includes("/property/device/luoxunthreephase/") || id.includes("/property/device/luoxunwaterpos/")
                || id.includes("/property/device/luoxunwaterpressure/")
              ) {
                return 'views_property_device_luoxun'
              } else if (id.includes("/property/device/spray/")) {
                return 'views_property_device_spray'
              } else if (id.includes("/property/device/spraymainboard/") || id.includes("/property/device/watermeter/")) {
                return 'views_property_device_spraymainboard'
              } else if (id.includes("/property/device/slurryscraper/")) {
                return 'views_property_device_slurryscraper'
              } else if (id.includes("/property/device/drinkhearting/") || id.includes("/property/device/drinkingstation/")) {
                return 'views_property_device_drinkheartingstation'
              } else if (id.includes("/property/device/rollershades/")) {
                return 'views_property_device_rollershades'
              } else {
                return 'views_property_device_main'
              }
            } else if (id.includes("/views/property/cattlemanager/")) {
              if (id.includes("/property/cattlemanager/cattle")) {
                return 'views_property_cattlemanager_cattle';
              } else if (id.includes("/property/cattlemanager/herd")) {
                return 'views_property_cattlemanager_herd';
              } else {
                return 'views_property_cattlemanager_main';
              }
            } else if (id.includes("/views/property/managerdevice/")) {
                return 'views_property_managerdevice_main';
            } else if (id.includes("/api/property/")) {
              if (id.includes("/api/property/milker/")) {
                return 'api_property_milker_sets';
              } else if (id.includes("/api/property/cattlemanager/")) {
                return 'api_property_cattlemanager_sets';
              } else {
                return 'api_property_sets'
              }
            }
          }
        },
      }
    }
  };
});
